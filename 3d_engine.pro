TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

SOURCES += main.cpp \
    camera.cpp \
    renderer.cpp \
    video.cpp \
    triangle.cpp \
    triangle2d.cpp \
    input.cpp

HEADERS += \
    camera.h \
    renderer.h \
    video.h \
    triangle.h \
    triangle2d.h \
    input.h


unix|win32: LIBS += -lassimp

unix|win32: LIBS += -lSDL

unix|win32: LIBS += -lSDL_gfx
