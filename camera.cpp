#include "camera.h"
#include <QtMath>

Camera::Camera():
    yaw(0),
    pitch(0),
    roll(0)
{
    Update();
}

void Camera::SetPosition(const QVector3D &pos) {
    position = pos;
}

void Camera::SetYaw(const float angle) {
    yaw = angle;
    if (yaw > 360) {
        yaw -= 360;
        return;
    }
    if (yaw < 0) {
        yaw += 360;
        return;
    }
}

void Camera::SetPitch(const float angle) {
    pitch = angle;
    if (pitch > 360) {
        pitch -= 360;
        return;
    }
    if (pitch < 0) {
        pitch += 360;
        return;
    }
}

void Camera::SetRoll(const float angle) {
    roll = angle;
    if (roll > 360) {
        roll -= 360;
        return;
    }
    if (roll < 0) {
        roll += 360;
        return;
    }
}

void Camera::Update() {
    float _yaw = qDegreesToRadians(yaw);
    float sinYaw = qSin(_yaw);
    float cosYaw = qCos(_yaw);

    QMatrix3x3 rotY;
    rotY(0, 0) = cosYaw;
    rotY(0, 1) = 0;
    rotY(0, 2) = sinYaw;

    rotY(1, 0) = 0;
    rotY(1, 1) = 1;
    rotY(1, 2) = 0;

    rotY(2, 0) = -sinYaw;
    rotY(2, 1) = 0;
    rotY(2, 2) = cosYaw;

    float _pitch = qDegreesToRadians(pitch);
    float sinPitch = qSin(_pitch);
    float cosPitch = qCos(_pitch);

    QMatrix3x3 rotX;
    rotX(0, 0) = 1;
    rotX(0, 1) = 0;
    rotX(0, 2) = 0;

    rotX(1, 0) = 0;
    rotX(1, 1) = cosPitch;
    rotX(1, 2) = -sinPitch;

    rotX(2, 0) = 0;
    rotX(2, 1) = sinPitch;
    rotX(2, 2) = cosPitch;

    float _roll = qDegreesToRadians(roll);
    float sinRoll = qSin(_roll);
    float cosRoll = qCos(_roll);

    QMatrix3x3 rotZ;
    rotZ(0, 0) = cosRoll;
    rotZ(0, 1) = -sinRoll;
    rotZ(0, 2) = 0;

    rotZ(1, 0) = sinRoll;
    rotZ(1, 1) = cosRoll;
    rotZ(1, 2) = 0;

    rotZ(2, 0) = 0;
    rotZ(2, 1) = 0;
    rotZ(2, 2) = 1;

    rotation = rotZ * rotX * rotY;
}

QVector3D Camera::Forward() const {
    return QVector3D(-rotation(0, 2), -rotation(1, 2), -rotation(2, 2));
}

QVector3D Camera::Right() const {
    return QVector3D(rotation(0, 0), rotation(1, 0), rotation(2, 0));
}

QVector3D Camera::Up() const {
    return QVector3D(rotation(0, 1), rotation(1, 1), rotation(2, 1));
}

const QMatrix4x4 Camera::ModelView() const {
    QMatrix4x4 translation;
    translation.setToIdentity();
    translation.setColumn(3, QVector4D(-position, 1));
    return QMatrix4x4(rotation.transposed()) * translation;
}

float Camera::Yaw() const {
    return yaw;
}

float Camera::Pitch() const {
    return pitch;
}

float Camera::Roll() const {
    return roll;
}

const QVector3D& Camera::Position() const {
    return position;
}

const QMatrix3x3& Camera::Rotation() const {
    return rotation;
}
