#ifndef CAMERA_H
#define CAMERA_H

#include <QVector3D>
#include <QMatrix4x4>

class Camera
{
public:
    Camera();
    void SetPosition(const QVector3D& pos);
    void SetYaw(const float angle);
    void SetPitch(const float angle);
    void SetRoll(const float angle);

    void Update();

    QVector3D Forward() const;
    QVector3D Right() const;
    QVector3D Up() const;
    float Yaw() const;
    float Pitch() const;
    float Roll() const;
    const QVector3D& Position() const;
    const QMatrix3x3& Rotation() const;
    const QMatrix4x4 ModelView() const;

private:
    float       yaw, pitch, roll;
    QMatrix3x3  rotation;
    QVector3D   position;
};

#endif // CAMERA_H
