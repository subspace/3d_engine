#include "input.h"
#include <SDL/SDL.h>

Input::Input(Camera &cam, bool &quitFlag):
    cam(cam),
    quitFlag(quitFlag)
{
}


void Input::ReadInput() {
    SDL_Event event;
    static signed char strafing = 0, movingForward = 0, rolling = 0, jumping = 0, turning = 0, looking = 0;

    while(SDL_PollEvent(&event)) {
        switch(event.type) {
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym) {
            case SDLK_a:
                strafing = -1;
                break;
            case SDLK_d:
                strafing = 1;
                break;
            case SDLK_s:
                movingForward = -1;
                break;
            case SDLK_w:
                movingForward = 1;
                break;
            case SDLK_q:
                rolling = -1;
                break;
            case SDLK_e:
                rolling = 1;
                break;
            case SDLK_SPACE:
                jumping = 1;
                break;
            case SDLK_LALT:
                jumping = -1;
                break;
            case SDLK_LEFT:
                turning = 1;
                break;
            case SDLK_RIGHT:
                turning = -1;
                break;
            case SDLK_UP:
                looking = 1;
                break;
            case SDLK_DOWN:
                looking = -1;
                break;
            case SDLK_ESCAPE:
                quitFlag = true;
                break;
            default:
                break;
            }
            break;
        case SDL_KEYUP:
            switch(event.key.keysym.sym) {
            case SDLK_a:
                strafing = 0;
                break;
            case SDLK_d:
                strafing = 0;
                break;
            case SDLK_s:
                movingForward = 0;
                break;
            case SDLK_w:
                movingForward = 0;
                break;
            case SDLK_q:
                rolling = 0;
                break;
            case SDLK_e:
                rolling = 0;
                break;
            case SDLK_SPACE:
                jumping = 0;
                break;
            case SDLK_LALT:
                jumping = 0;
                break;
            case SDLK_LEFT:
                turning = 0;
                break;
            case SDLK_RIGHT:
                turning = 0;
                break;
            case SDLK_UP:
                looking = 0;
                break;
            case SDLK_DOWN:
                looking = 0;
                break;
            default:
                break;
            }
            break;
        case SDL_QUIT:
            quitFlag = true;
            return;
        }
    }

    if (turning) {
        cam.SetYaw(cam.Yaw() + turning);
    }
    if (looking) {
        cam.SetPitch(cam.Pitch() + looking);
    }
    if (rolling) {
        cam.SetRoll(cam.Roll() + rolling);
    }

    if (movingForward) {
        cam.SetPosition(cam.Position() + movingForward*cam.Forward());
    }
    if (strafing) {
        cam.SetPosition(cam.Position() + strafing*cam.Right());
    }
    if (jumping) {
        cam.SetPosition(cam.Position() + jumping*cam.Up());
    }
    cam.Update();
}
