#ifndef INPUT_H
#define INPUT_H

#include "camera.h"

class Input
{
public:
    Input(Camera& cam, bool &quitFlag);
    void ReadInput();
private:
    Camera& cam;
    bool &quitFlag;
};

#endif // INPUT_H
