#include <iostream>
#include "video.h"
#include "renderer.h"
#include "input.h"

using namespace std;

int main() {
    bool        quit = false;
    Video       video;
    Renderer    renderer(video);
    Input       input(renderer.camera(), quit);

    video.Init(800, 600);
    renderer.SetViewport(0, 0, 800, 600);
    renderer.SetFrustrum(90, 1, 4092, 600/800.0f);

    while(!quit) {
        input.ReadInput();
        renderer.RenderScene();
    }
    return 0;
}

