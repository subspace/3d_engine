#include "renderer.h"
#include "camera.h"
#include <QtMath>

#include <SDL/SDL_gfxPrimitives.h>

#include <QDebug>

Renderer::Renderer(Video &video):
    video(video)
{
    vislist.push_back(Triangle(QVector3D(-10.0f, 0, -100.0f), QVector3D(10.0f, 0, -100.0f), QVector3D(0, 10.0f, -100.0f)));
}

void Renderer::SetFrustrum(float fovX, float near, float far, float aspect) {
    float plane_width = qTan(qDegreesToRadians(fovX/2.0f))*near;
    float plane_height = plane_width * aspect;

    clipmatrix.setToIdentity();
    clipmatrix(0, 0) = near/plane_width;
    clipmatrix(1, 1) = near/plane_height;
    clipmatrix(2, 2) = (near+far)/(near-far);
    clipmatrix(2, 3) = (2*near*far)/(near-far);
    clipmatrix(3, 2) = -1;
    clipmatrix(3, 3) = 0;
}

void Renderer::SetViewport(int x, int y, int width, int height) {
    viewportmatrix.fill(0);
    viewportmatrix(0, 0) = (width - 1) / 2.0f;
    viewportmatrix(0, 3) = (width - 1) / 2.0f;
    viewportmatrix(1, 1) = -(height - 1) / 2.0f;
    viewportmatrix(1, 3) = (height - 1) / 2.0f;
}

static inline bool IsInFrustrum(const QVector4D& point) {
    if (point.x() > point.w() || point.x() < -point.w()) {
        return false;
    }
    if (point.y() > point.w() || point.y() < -point.w()) {
        return false;
    }
    if (point.z() > point.w() || point.z() < -point.w()) {
        return false;
    }
    return true;
}

void Renderer::DrawCrosshair() {
    lineRGBA(video.Screen(), video.Screen()->w/2.0f-3, video.Screen()->h/2.0f, video.Screen()->w/2.0f+4, video.Screen()->h/2.0f, 255, 0, 0, 255);
    lineRGBA(video.Screen(), video.Screen()->w/2.0f-3, video.Screen()->h/2.0f+1, video.Screen()->w/2.0f+4, video.Screen()->h/2.0f+1, 255, 0, 0, 255);
    lineRGBA(video.Screen(), video.Screen()->w/2.0f, video.Screen()->h/2.0f-3, video.Screen()->w/2.0f, video.Screen()->h/2.0f+4, 255, 0, 0, 255);
    lineRGBA(video.Screen(), video.Screen()->w/2.0f+1, video.Screen()->h/2.0f-3, video.Screen()->w/2.0f+1, video.Screen()->h/2.0f+4, 255, 0, 0, 255);
}

void Renderer::RenderScene() {
    video.Clear();
    QMatrix4x4 transforms = clipmatrix * cam.ModelView();

    for (int i = 0; i < vislist.size(); ++i) {
        const Triangle& tri = vislist.at(i);

        QVector4D a(transforms * QVector4D(tri.A(), 1));
        QVector4D b(transforms * QVector4D(tri.B(), 1));
        QVector4D c(transforms * QVector4D(tri.C(), 1));

        // Test if all vertices are in the view frustrum
        if (!IsInFrustrum(a) || !IsInFrustrum(b) || !IsInFrustrum(c)) {
            continue;
        }

        a /= a.w();
        b /= b.w();
        c /= c.w();

        a = viewportmatrix * a;
        b = viewportmatrix * b;
        c = viewportmatrix * c;
        Triangle d(a.toVector3D(), b.toVector3D(), c.toVector3D());
        DrawTriangle(d);
    }
    DrawCrosshair();
    SDL_Flip(video.Screen());
}

void Renderer::DrawTriangle(const Triangle &tri) {
    trigonRGBA(video.Screen(), tri.A().x(), tri.A().y(), tri.B().x(), tri.B().y(), tri.C().x(), tri.C().y(), 255, 255, 255, 255);
}

Camera& Renderer::camera() {
    return cam;
}
