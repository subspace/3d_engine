#ifndef RENDERER_H
#define RENDERER_H

#include "camera.h"
#include "triangle.h"
#include "video.h"
#include <QList>

class Renderer
{
public:
    Renderer(Video& video);

    void RenderScene();
    void SetViewport(int x, int y, int width, int height);
    void SetFrustrum(float fovX, float near, float far, float aspect);
    void DrawTriangle(const Triangle& tri);
    void DrawCrosshair();
    Camera& camera();

private:
    Camera cam;
    QMatrix4x4 clipmatrix;
    QMatrix4x4 viewportmatrix;
    QList<Triangle> vislist;
    Video& video;
};

#endif // RENDERER_H
