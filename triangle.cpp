#include "triangle.h"

Triangle::Triangle(QVector3D a, QVector3D b, QVector3D c):
    a(a), b(b), c(c)
{
    normal = QVector3D::crossProduct(b - a, c - a).normalized();
}

const QVector3D& Triangle::A() const {
    return a;
}

const QVector3D& Triangle::B() const {
    return b;
}

const QVector3D& Triangle::C() const {
    return c;
}

const QVector3D& Triangle::Normal() const {
    return normal;
}

