#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <QVector3D>

class Triangle
{
public:
    Triangle(QVector3D a, QVector3D b, QVector3D c);
    const QVector3D& A() const;
    const QVector3D& B() const;
    const QVector3D& C() const;
    QVector3D Edge0() const;
    QVector3D Edge1() const;
    const QVector3D& Normal() const;

private:
    QVector3D a, b, c;
    QVector3D normal;
};

#endif // TRIANGLE_H
