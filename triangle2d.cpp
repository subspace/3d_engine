#include "triangle2d.h"

Triangle2D::Triangle2D(const QVector2D &a, const QVector2D &b, const QVector2D &c):
    a(a), b(b), c(c)
{
}

const QVector2D& Triangle2D::A() const {
    return a;
}

const QVector2D& Triangle2D::B() const {
    return b;
}

const QVector2D& Triangle2D::C() const {
    return c;
}

QVector2D Triangle2D::Edge0() const {
    return b - a;
}

QVector2D Triangle2D::Edge1() const {
    return c - a;
}
