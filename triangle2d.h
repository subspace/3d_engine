#ifndef TRIANGLE2D_H
#define TRIANGLE2D_H

#include <QVector2D>

class Triangle2D
{
public:
    Triangle2D(const QVector2D& a, const QVector2D& b, const QVector2D& c);
    const QVector2D& A() const;
    const QVector2D& B() const;
    const QVector2D& C() const;
    QVector2D Edge0() const;
    QVector2D Edge1() const;

private:
    QVector2D a, b, c;
};

#endif // TRIANGLE2D_H
