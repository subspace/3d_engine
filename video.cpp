#include "video.h"

Video::Video():
    screen(0)
{
}

Video::~Video() {
    Shutdown();
}

bool Video::Init(int w, int h) {
    if (screen)
        return false;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        return false;

    screen = SDL_SetVideoMode(w, h, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    if (!screen)
        return false;

    return true;
}

SDL_Surface* Video::Screen() {
    return screen;
}

void Video::Flip() {
    SDL_Flip(screen);
}

void Video::Clear() {
    SDL_Rect s;
    s.x = 0;
    s.y = 0;
    s.w = 800;
    s.h = 600;
    SDL_FillRect(screen, &s, 0);
}

void Video::Shutdown() {
    if (screen) {
        SDL_FreeSurface(screen);
        screen = 0;
    }
}
