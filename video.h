#ifndef VIDEO_H
#define VIDEO_H

#include <SDL/SDL.h>

class Video
{
public:
    Video();
    ~Video();
    bool Init(int w, int h);
    void Shutdown();
    SDL_Surface* Screen();
    void Flip();
    void Clear();

private:
    SDL_Surface* screen;
};

#endif // VIDEO_H
